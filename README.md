[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687157.svg)](https://doi.org/10.5281/zenodo.4687157)

# Hourly electricity prices 2015



## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation

This datasets includes hourly electricity prices in the year 2015 for EU28 on country level (NUTS0).  

The data set on hourly electricity prices represents day-ahead prices for the year 2015. 
Raw data was downloaded from the ENTSO-E Transparancy platform [1]. As within the Hotmaps toolbox typical years are used for most calculation models the data set is based on an index representing the hours of the year [1, 2 until 8760]. 
The time series is based on the Central European Time (CET) with hour #1 representing the first hour of the year in CET (01.01.2015 00:00 to 01:00). 
Hours where no value was available were replaced with prices from the same hour of the following day or the corresponding hour of the next day where data was available. 
All prices are given in €/MWh. As day ahead prices were not available for all countries covered by the Hotmaps toolbox, reference countries were used for those missing time series.
In countries with multiple price zones the price zones in which the capital city is located was chosen for the default data base in Hotmaps.

For assumptions on reference countries please see [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.8.1.2 page 137.

The hourly prices will be used to calculate the optimal dispatch of CHPs and large scale HPs in the DH module of the Hotmaps toolbox. Hourly price patterns will also be used to estimate the optimal dispatch of demand response ready technologies for individual heating systems.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.8 page 136ff.

### Limitations of the datasets

The hourly day-ahead prices only represent wholesale market prices. Wholesale prices only account for a rather small share of end-user prices, which also include grid costs, taxes and other surcharges. This has to be considered when assessing generation costs for heating with HPs or direct electric heaters both from the perspective of households and services as well as from the perspective of DH operators. The same holds for assess the value or potential revenue of electricity generation from CHPs within the Hotmaps toolbox when using default day-ahead prices provided in the toolbox. 
Wholesale prices for the countries where no price data was available might deviate significantly from the prices of the reference countries used to set up the default data on electricity prices. It also has to be noted that electricity prices depend on various exogenous parameters such as fossil fuel prices, CO2 prices, weather year or the installed capacity of renewables, which have all been subject to rather dynamic development in recent years. Therefore, the prices provided in this data set have to be interpreted as a snapshot of the conditions in the year 2015. It is highly recommended to use additional price information and sensitivity runs when assessing technologies and costs based on electricity prices within the Hotmaps toolbox.




### References
[1] [ENTSO-E - Transparancy platform](https://transparency.entsoe.eu/transmission-domain/r2/dayAheadPrices/show)



## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 


## Authors
Michael Hartner <sup>*</sup>,

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/)
Institute of Energy Systems and Electrical Drives
Gusshausstrasse 27-29/370
1040 Wien


## License
For Licensing we refer to the terms of use of the [ENTSO-E Transparency Platform](https://transparency.entsoe.eu/content/static_content/Static%20content/terms%20and%20conditions/terms%20and%20conditions.html)  

## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.